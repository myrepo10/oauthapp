package com.myapp.oauth.auth.authController;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TokenResponseDto {
    public TokenResponseDto(String id, String token){
        this.id = id;
        this.token = token;
    }
    String id;
    String token;

}
