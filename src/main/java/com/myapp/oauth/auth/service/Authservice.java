package com.myapp.oauth.auth.service;

import com.myapp.oauth.auth.authController.TokenResponseDto;
import com.myapp.oauth.auth.repository.DataEntity;
import com.myapp.oauth.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class Authservice {

    @Autowired
    private UserRepository ur;

    public String validateToken(String token) {
        DataEntity user = ur.findByToken(token);
        if (user == null) {
            return "false";
        } else {
            return "true";
        }
    }

    public Iterable<DataEntity> getAll() {
        return ur.findAll();
    }

    public String userLoginHandle(String username, String email, String password) {
        DataEntity reqUser = ur.findByUsernameAndEmailAndPassword(username, email, password);
        if (reqUser == null) {
            return null;
        } else {
            String token = UUID.randomUUID().toString();
            reqUser.setToken(token);
            ur.save(reqUser);
            //return token
            String redirectUrl = "/resources?auth=" + reqUser.getToken();
            return redirectUrl;
        }
    }

    public TokenResponseDto userRegister(String username, String email, String password){
        DataEntity user = new DataEntity(username, email, password);
        String token = UUID.randomUUID().toString();
        user.setToken(token);
        ur.save(user);
        return new TokenResponseDto( email ,token);
    }
}
