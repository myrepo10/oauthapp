package com.myapp.oauth.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<DataEntity,Long> {
    DataEntity findByEmail(String email);
    DataEntity findByToken(String token);
    DataEntity findByUsernameAndEmailAndPassword(String username, String email, String password);
}