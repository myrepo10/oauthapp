package com.myapp.oauth.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.client.RestTemplate;

@RestController
public class ResourceController {
    @GetMapping("/resources")
    public String returnData(@RequestParam String auth){
        final String uri = "http://localhost:8080/validate-token?token=" + auth;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        if (result.contains("true")){
            return "Some very confidential data xD" ;
        }
        else{
            return "403:Access Denied";
        }
    }

}